import json


class Word_choice:
    """Поиск по маске - это сервис, призванный прийти на помощь,
    если известна длина слова и некоторые буквы, в этом случае вы заменяете пропущенные
    буквы звездочками и вводите их, после чего программа подберет все слова, удовлетворяющие этому условию
    которые есть в нашей базе. Как правило, одной маске может соответствовать множество слов,
    к примеру маске 'т**л**й**с' соответствует более десяти слов.
    Сервис пригодится при разгадывании кроссвордов, сканвордов и всевозможных ребусов со словами."""

    __author__ = "Морозов Денис"
    __copyright__ = "Copyright 2021,Морозов Денис"
    __version__ = "1.0.3"
    __email__ = "TSETM@yandex.ru"
    __status__ = "Production"

    def __init__(self):
        self.end, self.beg, self.cen, self.cen_ = [], [], [], []
        self.result_info = ''
        self.file_data = ''  # Переменная, в которую будет записан список с данными.
        # Создаем пустые списки.
        self.suitable_words = []
        self.symbols = []  # Здесь мы будем хранить известные символы.
        self.ind_symbols = []  # Здесь мы будем хранить индекс известного нам символа.
        print('Введите слово, заменяя неизвестные буквы символом знаком *(звездочка),'
              ' т.е. слово "СОБАКА", может выглядеть как: **БА**.')
        text = input('>>> ').lower()
        if text != '':
            self.position_symbol(text)
            self.perform_the_selection_of_words(text)
        else:
            print('Извини, но ты ничего не написал.')

    def position_symbol(self, text):
        # мы выясняем, в каких позициях мы знаем символы.
        ind = 0  # Индекс по умолчанию равен нулю.
        # В цикле посимвольно проходимся по переданому аргументу.
        for symbol in text:
            if symbol != '*':
                # Добавляем идекс символа в список, и сам символ.
                self.ind_symbols.append(ind)
                self.symbols.append(symbol)
                ind += 1  # Увеличиваем значение индекса на 1 единицу.
            else:
                ind += 1  # Увеличиваем значение индекса на 1 единицу.

        self.info(text)

    def info(self, text):
        # Выясняем середины полученного слова.
        if len(text) <= 3:
            self.cen_ = 1
            for i in self.ind_symbols:
                if i < self.cen_:
                    # Если индекс символа меньше центра, то символ находится в начале слова
                    self.beg.append(text[i])
                elif i > self.cen_:
                    # Если индекс символа больше центра, то символ находится в конце слова.
                    self.end.append(text[i])
                else:
                    # Если индекс символа равен центру, то символ находится в середине слова.
                    self.cen.append(text[i])
        else:
            self.cen_ = [len(text) // 2, len(text) // 2 - 1]
            for i in self.ind_symbols:
                if i < self.cen_[1]:
                    # Если индекс символа меньше центра, то символ находится в начале слова
                    self.beg.append(text[i])
                elif i > self.cen_[0]:
                    # Если индекс символа больше центра, то символ находится в конце слова.
                    self.end.append(text[i])
                else:
                    # Если индекс символа равен центру, то символ находится в середине слова.
                    self.cen.append(text[i])

        if len(self.beg) != 0 and len(self.cen) != 0 and len(self.end) != 0:
            self.result_info = f'Слова из {len(text)} букв, начинающиеся на буквы {" ".join(self.beg)}, содержащие в ' \
                               f'середине буквы {" ".join(self.cen)}, в конце {" ".join(self.end)} '
        elif len(self.beg) != 0 and len(self.cen) != 0:
            self.result_info = f'Слова из {len(text)} букв, начинающиеся на буквы {" ".join(self.beg)}, и содержащие в ' \
                               f'середине буквы {" ".join(self.cen)} '
        elif len(self.cen) != 0 and len(self.end) != 0:
            self.result_info = f'Слова из {len(text)} букв, содержащие в середине буквы {" ".join(self.cen)}, ' \
                               f'заканчивающиеся на {" ".join(self.end)} '
        elif len(self.beg) == 0 and len(self.cen) != 0 and len(self.end) == 0:
            self.result_info = f'Слова из {len(text)} букв, содержащие в ' \
                               f'середине буквы {" ".join(self.cen)} '
        elif len(self.beg) == 0 and len(self.cen) == 0 and len(self.end) != 0:
            self.result_info = f'Слова из {len(text)} букв, в конце {" ".join(self.end)} '
        elif len(self.beg) != 0 and len(self.cen) == 0 and len(self.end) == 0:
            self.result_info = f'Слова из {len(text)} букв, начинающиеся на буквы {" ".join(self.beg)} '
        else:
            self.result_info = f'Слова из {len(text)} букв, начинающиеся на буквы {" ".join(self.beg)}, ' \
                               f'заканчивающиеся на {" ".join(self.end)}'

    def perform_the_selection_of_words(self, text):
        # Читаем файл с данными.
        with open('words.json') as file:
            self.file_data = json.load(file)
        # Через цикл ищем подходящие слова по известным нам символам.
        for i in self.file_data[str(len(text))]:
            coincidences = 0
            for u in self.ind_symbols:
                if i[u] == self.symbols[coincidences]:
                    coincidences += 1
                # Выполняем проверку на количество совпадений символов.
                # Если количества совпадений НЕ равна количеству известных нам символов,
                # Слова не записываем в список.
                if coincidences == len(self.symbols):
                    self.suitable_words.append(i)

        # Вывод списка слов.
        if len(self.suitable_words) != 0:
            print(f'{self.result_info}, найдено слов {len(self.suitable_words)} шт')
            print(self.suitable_words)
        else:
            print('Мы не нашли никаких совпадений на маске')


if __name__ == '__main__':
    Word_choice()
